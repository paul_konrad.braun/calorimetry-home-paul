from functions import m_json
from functions import m_pck

# test commit print("")
#m_pck.check_sensors()
path = "/home/pi/calorimetry-home-paul/datasheets/setup_newton.json"
data_folder = "/home/pi/calorimetry-home-paul/data/data_newton"
path2 = "/home/pi/calorimetry-home-paul/datasheets/"
metadata = m_json.get_metadata_from_setup(path)
m_json.add_temperature_sensor_serials(path2, metadata)
#print(metadata)
data = m_pck.get_meas_data_calorimetry(metadata)
m_pck.logging_calorimetry(data, metadata, data_folder, path2)
m_json.archiv_json(path2, path, data_folder)

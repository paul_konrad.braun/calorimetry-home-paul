# Lerneinheit II-I: Kalorimetrie (Küchentisch)
## Einführung
Siehe Skript und Aufgabenstellung in [moodle](https://moodle.tu-darmstadt.de/course/view.php?id=36368&section=5#tabs-tree-start).

## Materialien
In diesem GitLab Repo finden Sie:
- Package functions (`functions/`): Beinhaltet die Module `m_json`, `m_pck` und Skript `s_uuid6`
- Modul `m_json` (`functions/m_json.py`): Vorlage der Funktionen zum Auslesen, Archivieren und zur Bearbeitung der Metadaten
- Modul `m_pck` (`functions/m_pck.py`): Vorlage der Funktionen zum Aufnahmen uns Abspeichern der Messungsdaten
- Skript `s_uuid6` (`functions/s_uuid6.py`): Um uuid zu generieren
- Python-Hilfsdatei (`functions/__init__.py`): Notwendige Datei für die Erzeugung eines Python-Pakets
- Datenblätter (`datasheets/`): Vorlage der Datenblätter der Komponenten für Küchentischversuch im JSON-Format
- Messdaten (`data/`): Beinhaltet die Ordner der Messdaten
- Abbildungsordner (`figures/`): Ordner zum Ablegen die Fotos bzw. Abbildungen aus der Auswertung
- Notebook Kapazität (`ausarbeitung_kapazitaet.ipynb`): Vorlage zur Messdatenauswertung für Kapazität
- Notebook Newtonsches Abkühlungsgesetz (`ausarbeitung_newton.ipynb`): Vorlage zur Messdatenauswertung für das Newtonsche Abkühlungsgesetz
- Main Skript (`main.py`): Vorlage zum Aufnahmen der Messdaten
- Infosblatt (`DS18B20_Datasheet.pdf`): Infosblatt des DS18B20 Sensors
- Readme (`REAMDE.md`): diese Datei
- Requirements (`requirements.txt`): Beschreibt die pip-Umgebung, nicht relevant für die Ausarbeitung
- Matplotlib Style (`FST.mplstyle`): Einstellung für Matplotlib nach der FST-Institut-Vorschrift

### H5py-Einführung
Es ist angenommen, dass eine Dateistruktur wie folgt vorliegt.
```
├── experiment/
│   ├── main.py
│   └── data/
│       └── example.h5
```
`example.h5` beinhaltet eine Gruppe und zwei Datensätze.  
```
example
    group1 (+ Attribute: authors)
        dataset2 (+ Attribute: description)
    dataset1
```
Einige häufig gebrauchte Funktionen von `h5py` Modul werden in `main.py` aufgerufen.
```python
import h5py as h5

# read example.h5 file and save it to variable f.
with h5.File("data/example.h5") as f:
    # save group1 in example.h5 to variable g.
    g = f["group1"]
    # read group1 attribute authors.
    authors = g.attrs["authors"]
    # read dataset2 attribute description.
    desc = f["group1/dataset2"].attrs["description"]
    # read first element of dataset1 and print.
    print(f["dataset1"][0])

# open example.h5 for write data in to.
f = h5.File("data/example.h5", "a")
# create group in group1
g = f.create_group("group1/subgroup1")
# create dataset dataset3 and read some data
d = g.create_dataset("dataset3", data=[1, 2, 3, 5])
# write attribute description in dataset3
d.attrs["description"] = "new dataset"
# close file
f.close()
```
Diese Einführung dient nur zu einer Erinnerung. Für detaillierte Beschreibung aller Funktionalität ist offizielle Dokumentation empfehlenswert.

### Datenstruktur
Messdaten und Metadaten existieren nicht nur in der Festplatte, sondern auch während der Durchführung des Programms im Arbeitsspeicher. Die Daten, die während der Durchführung des Programms eingelesen aus erstellt werden, bezeichnet man als `Runtime Daten`. Die Funktionen, die Sie bei der Bearbeitung implementieren werden, wurden unter Berücksichtigung der spziellen Datenstrukturen implementiert.
#### Runtime Metadaten
Die Datenstruktur basiert auf Python-Dictionary `dict`. `all` bezieht sich auf alle Komponenten des Prüfstandes. Das Value davon ist ein Python-Dictionary, das hat 2 Keys `values` und `names`. Die beinhalten jeweils eine Liste mit den UUIDs und Namen. 

Das Value von Key `setup_path` muss den Pfad zur Setup-Datei entsprechen, aus der das Metadaten-Dictionary erzeugt wird.

Weitere Keys entsprechen die Namen der Typen der Komponenten. Das Vaule ist jeweils ein Python-Dictionary mit gleich Key-Value-Paar wie das Vaule von `all` Key. `sensor` Typ ist einer Sonderfall. Das Value-Dictionary kann noch `serials` als Key mit einer Serials-Liste als Value enthalten.

```python
{
    'all': {
        'values': [list of all UUIDs],
        'names': [list of all names]
    },
    'setup_path': '/path/to/setup_up.json',
    # Only for sensor type, there is serials key
    'sensor': {
        'values': [list of UUIDs of sensors],
        'names': [list of names of sensors],
        'serials': [list of serials of sensors]
    },
    'type1': {
        'values': [list of UUIDs of type1],
        'names': [list of names of type1]
    },
    'type2': {
        'values': [list of UUIDs of type2],
        'names': [list of names of type2]
    },
    ...
}
```
#### Runtime Messdaten
Die Messdaten aller Sensoren werden während des Versuches in ein Python-Dictionary bzw. Liste abgespeichert. Das Dictionary hat UUIDs der Sensoren als Keys. Die entsprechenden Values sind Liste, die zwei Listen enthalten. Die erste Liste enthält die Temperaturdaten, während die zweite die Zeitpunten behalten.

```python
{
    'uuid1': [
        [list of temperature],
        [list of time step]
    ],
    'uuid2': [
        [list of temperature],
        [list of time step]
    ],
    'uuid3': [
        [list of temperature],
        [list of time step]
    ],
    ...
}
```
#### HDF5 structure
Die Messdaten werden am Ende der Versuche in HDF5-Daten abgespeichert. Die Daten müssen eine bestimmte Struktur aufweisen. In jeder HDF5-Datei existiert eine Sub-Gruppe `RawData`, die die weiteren als UUID benennten Sub-Gruppen enthält. Die Sub-Gruppen in `RawData` beinhalten zwei Datensätze mit den Namen `temperature` und `time`.

Die Metadaten `authors`, `created`, `experiment` und `group_number` sollten als Attribute der HDF5-Datei gespeichert werden. Die Attribute der Sub-Gruppen in `RawData` sind der Name und die Serial-Nummer des ensprechenden Sensores. Die Datasätze `time` beinhalten die Attribute über die verwendete Zeitkonvention. In diesem Fall sind folgende Attribute zu verwenden. 
``` python
{
    "time_convention": "Unixzeit",
    "the_epoch": "Donnerstag, der 1. Januar 1970, 00:00 Uhr UTC",
    "time_explanation": "Die Unixzeit zaehlt die vergangenen Sekunden seit The Epoch"
}
```
```
H5-File-Name (+ Attributes: authors, created[time], experiment, groupNumber, testrig)
    RawData
        UUID1 (+ Attributes: name, serial)
            temperature (this is a data set)
            timestamp (this is a data set)
        UUID2 (+ Attributes: name, serial)
            temperature (this is a data set)
            timestamp (this is a data set)
        UUID3 (+ Attributes: name, serial)
            temperature (this is a data set)
            timestamp (this is a data set)
        ...
```
### Links
[h5py Quick Start Guide](https://docs.h5py.org/en/stable/quick.html)

[NumPy Fundamentals](https://numpy.org/doc/stable/user/basics.html)

[Matplotlib Pyplot Scatter](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.scatter.html#matplotlib.pyplot.scatter)

[PyPi: W1ThermSensor](https://pypi.org/project/w1thermsensor/)

## Ausarbeitung
Die Ausarbeitung erfolgt in den Modulen und Notebooks. In diesen ist bereits eine Gliederung vorgegeben.

## Abgabe
Die Abgabe erfolgt über [moodle](https://moodle.tu-darmstadt.de/mod/assign/view.php?id=1249198). Committen und pushen Sie zunächst Ihre Änderungen auf GitLab und laden Sie von dort Ihr gesamtes Repo als .zip-Datei herunter (ein direkter Download vom JupyterHub ist leider nicht möglich). Benennen Sie die .zip-Datei nach dem folgenden Schema:

<p style="text-align: center;"> &lt;Nachname&gt;_&lt;Vorname&gt;_&lt;MATR-NR&gt;_&lt;GRUPPEN-NR&gt;_le_2-1.zip</p>

Abgaben, die diese Namenskonvention nicht erfüllen, können in der Bewertung nicht berücksichtigt werden.
Laden Sie diese .zip-Datei in moodle hoch. Insbesondere sollten vorhanden sein:
- Jupyter Notebooks mit Datenauswertungen
- Python-Module
